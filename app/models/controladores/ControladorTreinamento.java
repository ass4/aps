package models.controladores;

import models.Treinamento;
import models.cadastros.CadastroTreinamento;

import com.avaje.ebean.Page;

public class ControladorTreinamento {
	private CadastroTreinamento cadastroTreinamento;
	
	public ControladorTreinamento() {
		cadastroTreinamento = new CadastroTreinamento();
	}
	
	public void cadastrar(Treinamento treinamento){
		cadastroTreinamento.cadastrar(treinamento);
	}
	public void remover(Long id){
		cadastroTreinamento.remover(id);
	}

	public void atualizar(Treinamento treinamento){
		cadastroTreinamento.atualizar(treinamento);
	}
	/**
     * Retorna uma pagina de Treinamento
     *
     * @param page Page to display
     * @param pageSize Number of computers per page
     * @param sortBy Computer property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
	public Page<Treinamento> formatarTrenamento(int page, String sortBy, String order, String filter) {
		return 
            Treinamento.find.where()
                .ilike("nome", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .findPagingList(10)
                .setFetchAhead(false)
                .getPage(page);
	}
    
	
}
