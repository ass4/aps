# --- First database schema

# --- !Ups

create table company (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_company primary key (id))
;

create table computer (
  id                        bigint not null,
  name                      varchar(255),
  introduced                timestamp,
  discontinued              timestamp,
  company_id                bigint,
  constraint pk_computer primary key (id))
;

create table treinamento (
  id                        bigint not null,
  nome                      varchar(255),
  descricao                 varchar(255),
  aprovado		              bit,
  constraint pk_treinamento primary key (id))
;

create table feedback (
  id                        bigint not null,
  email                     varchar(255),
  pergunta                  varchar(255),
  resposta                  varchar(255),
  respondido                bit,
  constraint pk_feedback primary key (id))
;

create sequence treinamento_seq start with 1000;

create sequence feedback_seq start with 1000;

create sequence company_seq start with 1000;

create sequence computer_seq start with 1000;

alter table computer add constraint fk_computer_company_1 foreign key (company_id) references company (id) on delete restrict on update restrict;
create index ix_computer_company_1 on computer (company_id);




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists company;

drop table if exists computer;

drop table if exists treinamento;

drop table if exists feedback;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists company_seq;

drop sequence if exists computer_seq;

drop sequence if exists treinamento_seq;

drop sequence if exists feedback_seq;