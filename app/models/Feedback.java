package models;

import javax.persistence.*;

import play.db.ebean.	*;
import play.db.ebean.Model.Finder;
import play.data.validation.Constraints.Required;



@Entity
public class Feedback extends Model{
	@Id
	public Long id;
	
	@Required
	public String email;
	
	public String pergunta;
	
	public String resposta;
	
	public boolean respondido; 
	
	/**
     * Generic query helper para entidade Treinamento com id Long
     */
	public static Finder<Long,Feedback> find = new Finder<Long,Feedback>(Long.class, Feedback.class);
}
