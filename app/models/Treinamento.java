package models;

import javax.persistence.*;

import play.db.ebean.*;

import play.data.validation.Constraints.Required;




@Entity
public class Treinamento extends Model{
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;
	
	@Required
	public String nome;
	
	public String descricao;
	
	public boolean aprovado;

	/**
     * Generic query helper para entidade Treinamento com id Long
     */
	public static Finder<Long,Treinamento> find = new Finder<Long,Treinamento>(Long.class, Treinamento.class); 
}
