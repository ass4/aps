package models;
import models.controladores.ControladorFeedback;
import models.controladores.ControladorTreinamento;

import com.avaje.ebean.Page;

public class Fachada {
	
	// Controladores
	private ControladorTreinamento cTreinamento;
	private ControladorFeedback cFeedback;
	
	
	
	// Instancias que nao deverao ser recriadas
	private static Fachada instancia;
	
	
	// Construtor
	private Fachada(){
		cTreinamento = new ControladorTreinamento();
		cFeedback = new ControladorFeedback();
	}
	
	public static Fachada getInstance(){
		if(instancia == null){
			instancia = new Fachada();
		}
		return instancia;
	}
	
	// Operacoes da Fachada
	public void cadastrarTreinamento(Treinamento treinamento){
		cTreinamento.cadastrar(treinamento);
	}
	
	public void removerTreinamento(Long id){
		cTreinamento.remover(id);
	}
	
	public void atualizarTreinamento(Treinamento treinamento){
		cTreinamento.atualizar(treinamento);
	}
	
	public Page<Treinamento> formatarTreinamento(int page, String sortBy, String order, String filter){
		return cTreinamento.formatarTrenamento(page, sortBy, order, filter);
	}
	
	public Page<Feedback> formatarFeedback(int page, String sortBy, String order, String filter){
		return cFeedback.formatarFeedback(page, sortBy, order, filter);
	}
}
