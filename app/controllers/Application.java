package controllers;

import play.mvc.*;
import play.data.*;
import static play.data.Form.*;
import views.html.*;
import models.*;

/**
 * Personal Trainer Virtual
 */
public class Application extends Controller {


    /**
     * Este result redireciona diretamente para a pagina inicial
     */
    public static Result GO_HOME = redirect(
        routes.Application.listaTreinamento(0, "nome", "asc", "")
    );
    public static Result GO_HOME2 = redirect(
            routes.Application.listaFeedback(0, "email", "asc", "")
        );
    /**
     * Trata a requisicao do caminho padrao, redireciona para lista de treinamentos
     */
    public static Result index() {
        return GO_HOME;
    }

    /**
     * Mostra Lista de Treinamentos
     *
     * @param page Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order Sort order (either asc or desc)
     * @param filter Filter applied on computer names 
     */
    public static Result listaTreinamento(int page, String sortBy, String order, String filter) {
        return ok(
            treinamentoLista.render(
            		Fachada.getInstance().formatarTreinamento( page, sortBy, order, filter),
                sortBy, order, filter
            )
        );
    }

    /**
     * Mostra o formulario de Treinamento
     */
    public static Result criarTreinamento() {
        Form<Treinamento> treinamentoForm = form(Treinamento.class);
        return ok(
            treinamentoCriarForm.render(treinamentoForm)
        );
    }

    /**
     * Trata a submissao do formulario 'cirar treinamento'
     */
    public static Result salvarTreinamento() {
        Form<Treinamento> treinamentoForm = form(Treinamento.class).bindFromRequest();
        if(treinamentoForm.hasErrors()) {
            return badRequest(treinamentoCriarForm.render(treinamentoForm));
        }
        Treinamento treinamento = treinamentoForm.get();
        
        Fachada.getInstance().cadastrarTreinamento(treinamento);
        
        flash("success", "Treinamento " + treinamento.nome + " foi criado");
        return GO_HOME;
    }

    

    
    
    /**
     * Trata a remocao de um treinamento
     */
    public static Result removerTreinamento(Long id) {
        
    	Fachada.getInstance().removerTreinamento(id);
        
        flash("success", "Treinamento foi removido");
        return GO_HOME;
    }
    

    /**
     * Mostra o formulario de edicao
     *
     * @param id Id of the computer to edit
     */
    public static Result editarTreinamento(Long id) {
        Form<Treinamento> treinamentoForm = form(Treinamento.class).fill(
            Treinamento.find.byId(id)
        );
        return ok(
            treinamentoEditarForm.render(id, treinamentoForm)
        );
    }

    /**
     * Mostra o formulario de edicao
     *
     * @param id Id of the computer to edit
     */
    public static Result editarFeedback(Long id) {
        Form<Feedback> feedbackForm = form(Feedback.class).fill(
            Feedback.find.byId(id)
        );
        return ok(
            feedbackEditarForm.render(id, feedbackForm)
        );
    }

    /**
     * Trata a submissao do formulario 'editar treinamento'
     *
     * @param id Id of the computer to edit
     */
    public static Result atualizarTreinamento(Long id) {
        Form<Treinamento> treinamentoForm = form(Treinamento.class).bindFromRequest();
        //Form<Treinamento> treinamentoForm = form(Treinamento.class).fill(Treinamento.find.byId(id)).bindFromRequest();
        
        if(treinamentoForm.hasErrors()) {
            return badRequest(treinamentoEditarForm.render(id, treinamentoForm));
        }
        
        Treinamento treinamento = treinamentoForm.get();
        
        treinamento.update(id);
        //Fachada.getInstance().atualizarTreinamento(treinamento);
        

        flash("success", "Treinamento " + treinamento.nome + " foi atualizado");
        return GO_HOME;
    }
    /**
     * Trata a submissao do formulario 'editar feedback'
     *
     * @param id Id of the feedback to edit
     */
    public static Result atualizarFeedback(Long id) {
        Form<Feedback> feedbackForm = form(Feedback.class).bindFromRequest();
        
        if(feedbackForm.hasErrors()) {
            return badRequest(feedbackEditarForm.render(id, feedbackForm));
        }
        
        Feedback feedback = feedbackForm.get();
        
        feedback.update(id);
        //Fachada.getInstance().atualizarTreinamento(feedback);
        
        flash("success", "Feedback foi atualizado");
        return redirect(
                    routes.Application.listaFeedback(0, "email", "asc", "")
                );
    }

    /**
     * Mostra Lista de Treinamentos
     *
     * @param page Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order Sort order (either asc or desc)
     * @param filter Filter applied on feedback names 
     */
    public static Result listaFeedback(int page, String sortBy, String order, String filter) {
        return ok(
        	feedbackLista.render(
                    Fachada.getInstance().formatarFeedback( page, sortBy, order, filter),
                sortBy, order, filter
            )
        );
    }
}
            
/*

ler xml para configurar bd utilizado

forumlario editar treinamento - Aprovar ser um campo de check


*/

