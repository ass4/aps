package models.FabricaRep;
import java.io.File;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;


import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException; 


class leituraXML {
	
	static String type="";
	public static FabricaRep.tipoBD lerFabrica() {
		
		 try {

	            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	            Document doc = docBuilder.parse (new File("teste.xml"));

	            // normalize text representation
	            doc.getDocumentElement ().normalize ();
	            //System.out.println ("Root element of the doc is " +  doc.getDocumentElement().getNodeName());


	            NodeList listOfPersons = doc.getElementsByTagName("type");
	            int totalPersons = listOfPersons.getLength();
	            //System.out.println("Total no of people : " + totalPersons);

	            for(int s=0; s<listOfPersons.getLength() ; s++){


	                Node firstPersonNode = listOfPersons.item(s);
	                if(firstPersonNode.getNodeType() == Node.ELEMENT_NODE){


	                    Element firstPersonElement = (Element)firstPersonNode;

	                    //-------
	                    NodeList firstNameList = firstPersonElement.getElementsByTagName("name");
	                    Element firstNameElement = (Element)firstNameList.item(0);

	                    NodeList textFNList = firstNameElement.getChildNodes();
	                    System.out.println("First Name : " + 
	                           ((Node)textFNList.item(0)).getNodeValue().trim());
	                    
	                    type = ((Node)textFNList.item(0)).getNodeValue().trim();
	                    
	                    //o return é la embaixo

	                    //-------
	                   

	                }//end of if clause


	            }//end of for loop with s var


	        }catch (SAXParseException err) {
	        System.out.println ("** Parsing error" + ", line " 
	             + err.getLineNumber () + ", uri " + err.getSystemId ());
	        System.out.println(" " + err.getMessage ());

	        }catch (SAXException e) {
	        Exception x = e.getException ();
	        ((x == null) ? e : x).printStackTrace ();

	        }catch (Throwable t) {
	        t.printStackTrace ();
	        }
	        //System.exit (0);
		if(type.equals("H2")){return FabricaRep.tipoBD.H2;}  else return FabricaRep.tipoBD.ORACLE;
		
	}
	
}

public class FabricaRep {
	public static enum tipoBD{H2, ORACLE}
	
	public static IFabricaRep criarFabricaRep(){
		IFabricaRep fabrica = null;
		
		FabricaRep.tipoBD opcao = leituraXML.lerFabrica();
		
		switch(opcao){
			case H2:
				fabrica = new FabricaRepH2();
				break;
				
			case ORACLE:
				
				break;
		}
		
		return fabrica;
	}
}
