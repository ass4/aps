package models.cadastros;

import models.Feedback;

import com.avaje.ebean.Page;

public class CadastroFeedback {
	public Page<Feedback> getAllFeedbackNotAnswered(int page, String sortBy, String order, String filter){
		return Feedback.find.where()
				.ilike("email", "%" + filter + "%")
				.eq("respondido", false)
                .orderBy(sortBy + " " + order)
                .findPagingList(10)
                .setFetchAhead(false)
                .getPage(page);
	}
	
}
