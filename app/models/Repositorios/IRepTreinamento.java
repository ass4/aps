package models.Repositorios;

import java.util.List;

import models.Treinamento;

public interface IRepTreinamento {
	public List<Treinamento> all();
	
	public Treinamento findId(long id);
	
	public void cadastrar(Treinamento treinamento);
	
	public void remover(long id);
	
	public void atualizar(Treinamento treinamento, Long id);
}
