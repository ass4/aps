package models.FabricaRep;

import models.Repositorios.IRepTreinamento;
import models.Repositorios.RepTreinamentoH2;

public class FabricaRepH2 implements IFabricaRep{

	@Override
	public IRepTreinamento createRepTreinamento() {
		return new RepTreinamentoH2();
	}

}
