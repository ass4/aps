package models.controladores;

import models.Feedback;
import models.cadastros.CadastroFeedback;

import com.avaje.ebean.Page;

public class ControladorFeedback {
	private CadastroFeedback cadastroFeedback;
	public ControladorFeedback() {
		cadastroFeedback = new CadastroFeedback();
	}
	
	public Page<Feedback> formatarFeedback(int page, String sortBy, String order, String filter) {
		return cadastroFeedback.getAllFeedbackNotAnswered(page, sortBy, order, filter);
	}

}
